#!/bin/sh

set -e

bin_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"

: ${BIN_DIR:="/usr/local/bin"}
: ${DATA_DIR:="/var/run/dune-network"}

: ${NODE_HOST:="node"}
: ${NODE_RPC_PORT:="8733"}

: ${PROTOCOL:="unspecified-PROTOCOL-variable"}

node="$BIN_DIR/dune-node"
client="$BIN_DIR/dune-client"
admin_client="$BIN_DIR/dune-admin-client"
baker="$BIN_DIR/dune-baker-$PROTOCOL"
endorser="$BIN_DIR/dune-endorser-$PROTOCOL"
accuser="$BIN_DIR/dune-accuser-$PROTOCOL"
signer="$BIN_DIR/dune-signer"

client_dir="$DATA_DIR/client"
node_dir="$DATA_DIR/node"
node_data_dir="$node_dir/data"

. "$bin_dir/entrypoint.inc.sh"

command=${1:-dune-node}
shift 1

case $command in
    dune-node)
        launch_node "$@"
        ;;
    dune-upgrade-storage)
        upgrade_node_storage
        ;;
    dune-snapshot-import)
        snapshot_import "$@"
        ;;
    dune-baker)
        launch_baker "$@"
        ;;
    dune-baker-test)
        launch_baker_test "$@"
        ;;
    dune-endorser)
        launch_endorser "$@"
        ;;
    dune-endorser-test)
        launch_endorser_test "$@"
        ;;
    dune-accuser)
        launch_accuser "$@"
        ;;
    dune-accuser-test)
        launch_accuser_test "$@"
        ;;
    dune-client)
        configure_client
        exec "$client" "$@"
        ;;
    dune-admin-client)
        configure_client
        exec "$admin_client" "$@"
        ;;
    dune-signer)
        exec "$signer" "$@"
        ;;
    *)
        cat <<EOF
Available commands:
- dune-node [args]
- dune-client [args]
- dune-baker [keys]
- dune-baker-test [keys]
- dune-endorser [keys]
- dune-endorser-test [keys]
- dune-signer [args]
EOF
        ;;
esac
