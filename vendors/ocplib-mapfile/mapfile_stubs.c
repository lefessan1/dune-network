/************************************************************************/
/*                                Ironmin                               */
/*                                                                      */
/*  Copyright 2018-2019 OCamlPro                                        */
/*                                                                      */
/*  This file is distributed under the terms of the GNU General Public  */
/*  License as published by the Free Software Foundation; either        */
/*  version 3 of the License, or (at your option) any later version.    */
/*                                                                      */
/*  Ironmin is distributed in the hope that it will be useful,          */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/************************************************************************/

#define _LARGEFILE64_SOURCE

#include "caml/mlvalues.h"
#include "caml/fail.h"
#include "caml/alloc.h"
#include "caml/gc.h"
#include "caml/memory.h"
#include <caml/bigarray.h>

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

typedef struct {
  int fd ;
  unsigned char* addr;
  uint64_t map_size ;
  uint64_t current_size ;
} mapfile_t ;

#define INITIAL_SIZE_OFFSET 20

static void ocp_mapfile_increase_file(mapfile_t* db, uint64_t expected_size)
{
  expected_size += 10 ; /* always add at least 10 bytes at the end */
  if( expected_size > db->current_size ){
    off_t ret ;
    char buf[2];
    uint64_t final_size =
      (1 + (expected_size >> INITIAL_SIZE_OFFSET)) << INITIAL_SIZE_OFFSET ;
    if( final_size > db->map_size ){
      caml_failwith("Mapfile: map_size exceeeded");
    }
    /*    fprintf(stderr, "INCREASING SIZE TO %Ld > %Ld\n",
          final_size, expected_size); */
    ret = lseek( db->fd, final_size-1, SEEK_SET);
    if( ret < 0 ){ caml_failwith("ocp_mapfile_increase_file: bad lseek"); }
    buf[0] = 0;
    buf[1] = 0;
    ret = write(db->fd, buf, 1);
    if( ret < 0 ){ caml_failwith("ocp_mapfile_increase_file: bad write"); }
    db->current_size = final_size;
    /* fprintf(stderr, "INCREASED !\n"); */
  }
}

static void ocp_mapfile_check_size(mapfile_t* db, uint64_t expected_size)
{
  if( expected_size > db->current_size){
    db->current_size = lseek( db->fd, 0, SEEK_END );
    if( expected_size > db->current_size ){
      caml_failwith("Mapfile.read: file too short");
    }
  }
}


value ocp_mapfile_openfile_c (
                           value filename_v ,
                           value read_only_v ,
                           value map_size_v ,
                           value create_v
                           )
{
  CAMLparam2( filename_v, map_size_v);
  CAMLlocal1( db_v );
  size_t map_size = Int64_val( map_size_v );
  char *addr ;
  int read_only = Long_val( read_only_v );
  int create = Long_val ( create_v );
  unsigned char* filename = String_val( filename_v );
  int fd ;
  int map_flags;
  int open_flags;
  int open_mode = 420; /* 0o644 */
  uint64_t next_position;
  mapfile_t* db = NULL;
  uint64_t initial_size;
  
  open_flags = O_LARGEFILE;
  if( read_only )
    open_flags |= O_RDONLY ;
  else
    open_flags |= O_RDWR ;
  if( create ) open_flags |= O_CREAT;
  fd = open( filename, open_flags, open_mode );
  if( fd < 0 ) { caml_failwith("Mapfile.opendir: open failed"); }
  initial_size = lseek( fd, 0, SEEK_END );
  /*
  fprintf(stderr, "map_size %Ld\n", map_size);
  fprintf(stderr, "initial_size %Ld\n", initial_size);
  */
  if( initial_size > map_size ){
    close(fd);
    caml_failwith("Mapfile.opendir: map_size exceeeded");
  }
  map_flags = PROT_READ ;
  if ( ! read_only ) map_flags |= PROT_WRITE ;
  addr = mmap(NULL,
              map_size,
              map_flags,
              MAP_SHARED,
              fd,
              0);
  if( addr == NULL ){ caml_failwith("Mapfile.openfile: mmap failed"); }

  db = (mapfile_t*)malloc( sizeof(mapfile_t) );
  db->fd = fd;
  db->addr = addr;
  db->map_size = map_size;
  if( create ){
    db->current_size = 0;
    /* ocp_mapfile_increase_file(db, sizeof(file_t) ); */
  } else {
    db->current_size = initial_size;
  }
  /* fprintf(stderr, "current_size = %Ld\n", db->current_size); */
  db_v = caml_alloc( 1 , Abstract_tag );
  Field (db_v, 0) = (value) db;
  CAMLreturn( db_v ) ;
}

value ocp_mapfile_write_fixed_string_c ( value db_v ,
                                  value off_v ,
                                  value contents_v ,
                                  value pos_v ,
                                  value len_v )
{
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);
  uint64_t off = Long_val( off_v );
  unsigned char* contents = String_val( contents_v );
  uint64_t pos = Long_val ( pos_v );
  uint64_t len = Long_val ( len_v );
  uint64_t contents_len = caml_string_length( contents_v );

  if( db-> fd < 0 ){
    caml_failwith("Mapfile.write_fixed_string: already closed");
  }
  if( pos < 0 || len < 0 || off < 0 || pos + len > contents_len ){
    caml_failwith("Mapfile.write_fixed_string");
  }

  ocp_mapfile_increase_file( db, off + len );
  memcpy( db->addr + off,
          contents + pos,
          len);
  return Val_long( off + len );
}

value ocp_mapfile_write_string_c ( value db_v ,
                                  value off_v ,
                                  value contents_v ,
                                  value pos_v ,
                                  value len_v )
{
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);
  uint64_t off = Long_val( off_v );
  unsigned char* contents = String_val( contents_v );
  uint64_t pos = Long_val ( pos_v );
  uint64_t len = Long_val ( len_v );
  uint64_t contents_len = caml_string_length( contents_v );
  unsigned char* addr = db->addr;
  uint64_t initial_off = off;
  
  if( db-> fd < 0 ){
    caml_failwith("Mapfile.write_string: already closed");
  }
  if( pos < 0 || len < 0 || off < 0 || pos + len > contents_len ){
    caml_failwith("Mapfile.write_string");
  }

  ocp_mapfile_increase_file( db, off + 10 + len );
  {
    uint64_t size = len;
    while( size >= 128 ){
      addr[off++] = size & 0x7f ;
      size = size >> 7;
    }
    addr[off++] = size | 0x80 ;
  }
  memcpy( db->addr + off,
          contents + pos,
          len);
  off += len;
  if( off - initial_off < 8 ) off = initial_off + 8;
  return Val_long( off );
}


value ocp_mapfile_write_int_c ( value db_v ,
                                  value off_v ,
                                  value len_v )
{
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);
  uint64_t off = Long_val( off_v );
  uint64_t len = Long_val ( len_v );
  unsigned char* addr = db->addr;
  uint64_t initial_off = off;
  
  if( db-> fd < 0 ){
    caml_failwith("Mapfile.write_string: already closed");
  }
  if( len < 0 || off < 0 ){
    caml_failwith("Mapfile.write_string");
  }

  ocp_mapfile_increase_file( db, off + 10 );
  {
    uint64_t size = len;
    while( size >= 128 ){
      addr[off++] = size & 0x7f ;
      size = size >> 7;
    }
    addr[off++] = size | 0x80 ;
  }
  if( off - initial_off < 8 ) off = initial_off + 8;
  
  return Val_long( off );
}

value ocp_mapfile_write_fixed_bigstring_c ( value db_v ,
                                  value off_v ,
                                  value contents_v ,
                                  value pos_v ,
                                  value len_v )
{
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);
  uint64_t off = Long_val( off_v );
  unsigned char* contents = Caml_ba_data_val( contents_v );
  uint64_t pos = Long_val ( pos_v );
  uint64_t len = Long_val ( len_v );
  uint64_t contents_len = Caml_ba_array_val( contents_v )->dim[0];

  if( db-> fd < 0 ){
    caml_failwith("Mapfile.write_fixed_bigstring: already closed");
  }
  if( pos < 0 || len < 0 || off < 0 || pos + len > contents_len ){
    caml_failwith("Mapfile.write_fixed_bigstring");
  }

  ocp_mapfile_increase_file( db, off + len );
  memcpy( db->addr + off,
          contents + pos,
          len);
  return Val_long( off + len );
}

value ocp_mapfile_write_bigstring_c ( value db_v ,
                                  value off_v ,
                                  value contents_v ,
                                  value pos_v ,
                                  value len_v )
{
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);
  uint64_t off = Long_val( off_v );
  unsigned char* contents = Caml_ba_data_val( contents_v );
  uint64_t pos = Long_val ( pos_v );
  uint64_t len = Long_val ( len_v );
  uint64_t contents_len = Caml_ba_array_val( contents_v )->dim[0];
  unsigned char* addr = db->addr;

  if( db-> fd < 0 ){
    caml_failwith("Mapfile.write_bigstring: already closed");
  }
  if( pos < 0 || len < 0 || off < 0 || pos + len > contents_len ){
    caml_failwith("Mapfile.write_bigstring");
  }

  ocp_mapfile_increase_file( db, off + 10 + len );
  {
    uint64_t size = len;
    while( size >= 128 ){
      addr[off++] = size & 0x7f ;
      size = size >> 7;
    }
    addr[off++] = size | 0x80 ;
  }
  memcpy( db->addr + off,
          contents + pos,
          len);
  return Val_long( off + len );
}

value ocp_mapfile_size_c( value db_v )
{
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);
  if( db-> fd < 0 ){
    caml_failwith("Mapfile.size: already closed");
  }
  
  return Val_long( db->current_size );
}

value ocp_mapfile_commit_c ( value db_v, value sync_v )
{
  int ret;
  int sync = Long_val( sync_v );
  int flag = sync ? MS_SYNC : MS_ASYNC ;
    
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);
  if( db-> fd < 0 ){
    caml_failwith("Mapfile.commit: already closed");
  }
  ret = msync( db->addr, db->current_size, flag );
  if( ret < 0 ){
    caml_failwith("Mapfile.commit: bad msync");
  }
  return Val_unit ;
}

value ocp_mapfile_close_c ( value db_v )
{
  int ret;
    
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);

  if( db-> fd < 0 ){
    caml_failwith("Mapfile.close: already closed");
  }
  ret = msync( db->addr, db->current_size, MS_SYNC );
  if( ret < 0 ){
    caml_failwith("Mapfile.close: bad msync");
  }
  close( db->fd );
  db->fd = -1;
  return Val_unit ;
}

value ocp_mapfile_read_size_c ( value db_v ,
                                value off_v )
{
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);
  uint64_t off = Long_val( off_v );
  unsigned char* addr = db->addr;
  uint64_t len = 0;

  {
    unsigned char c;
    uint64_t lsl = 0;
    ocp_mapfile_check_size( db, off + 10 );
    c = addr[off++];
    while( c < 128 ){
      len += c << lsl;
      lsl += 7;
      c = addr[off++];
    }
    len += ( c & 0x7f ) << lsl;
  }

  return Val_long( len );
}

value ocp_mapfile_read_next_c ( value db_v ,
                                value off_v )
{
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);
  uint64_t off = Long_val( off_v );
  unsigned char* addr = db->addr;
  uint64_t len = 0;
  uint64_t initial_off = off ;
  
  {
    unsigned char c;
    uint64_t lsl = 0;
    ocp_mapfile_check_size( db, off + 10 );
    c = addr[off++];
    while( c < 128 ){
      len += c << lsl;
      lsl += 7;
      c = addr[off++];
    }
    len += ( c & 0x7f ) << lsl;
  }
  off += len ;
  if( off - initial_off < 8 ) off = initial_off + 8;
  
  return Val_long( off );
}

value ocp_mapfile_read_fixed_bigstring_c( value db_v, value off_v, value len_v )
{
  CAMLparam1( db_v );
  CAMLlocal1( ret_v );
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);
  unsigned char* addr = db->addr;
  uint64_t off = Long_val( off_v );
  uint64_t len = Long_val( len_v );

  ocp_mapfile_check_size( db, off + len );
  ret_v = caml_ba_alloc_dims(CAML_BA_UINT8 | CAML_BA_C_LAYOUT, 1,
                             addr + off, len) ;
  CAMLreturn( ret_v );
}

value ocp_mapfile_read_bigstring_c( value db_v, value off_v )
{
  CAMLparam1( db_v );
  CAMLlocal1( ret_v );
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);
  unsigned char* addr = db->addr;
  uint64_t off = Long_val( off_v );
  uint64_t len = 0 ;

  {
    unsigned char c;
    uint64_t lsl = 0;
    ocp_mapfile_check_size( db, off + 10 );
    c = addr[off++];
    while( c < 128 ){
      len += c << lsl;
      lsl += 7;
      c = addr[off++];
    }
    len += ( c & 0x7f ) << lsl;
  }

  ocp_mapfile_check_size( db, off + len );
  ret_v = caml_ba_alloc_dims(CAML_BA_UINT8 | CAML_BA_C_LAYOUT, 1,
                             addr + off, len) ;
  CAMLreturn( ret_v );
}

value ocp_mapfile_truncate_c(mapfile_t* db_v, uint64_t wanted_size_v)
{
  mapfile_t * db = (mapfile_t *) Field( db_v, 0);
  uint64_t wanted_size = Long_val( wanted_size_v );

  if( wanted_size < db->current_size ){
    int ret = ftruncate(db->fd, wanted_size);
    if( ret < 0 ){ caml_failwith("ocp_mapfile_truncate: bad ftruncate"); }
    db->current_size = wanted_size;
  }
  return Val_unit;
}
