(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

type ('key,'value) t = {
  fifo : 'key Queue.t ;
  table : ('key, 'value) Hashtbl.t;
  maxsize : int ;
  mutable size : int ;
  mutable adds : int ;
  clear_every : int ;
}
let create ?(clear_every = max_int) maxsize =
  {
    fifo = Queue.create () ;
    table = Hashtbl.create (2 * maxsize + 1);
    maxsize ;
    size = 0 ;
    adds = 0 ;
    clear_every ;
  }

let mem t key = Hashtbl.mem t.table key
let find t key = Hashtbl.find t.table key

let clear t =
  Queue.clear t.fifo;
  Hashtbl.clear t.table;
  t.size <- 0;
  t.adds <- 0

let add t key value =
  assert (not (Hashtbl.mem t.table key) );
  if t.size = t.maxsize then begin
    let oldkey = Queue.take t.fifo in
    Hashtbl.remove t.table oldkey
  end else
    t.size <- t.size + 1;
  Queue.add key t.fifo;
  Hashtbl.add t.table key value;
  t.adds <- t.adds + 1;
  if t.adds > t.clear_every then clear t
