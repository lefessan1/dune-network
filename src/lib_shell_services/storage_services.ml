(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Data_encoding

type keep_query = {
  keep: int option ;
}
let keep_query : keep_query RPC_query.t =
  let open RPC_query in
  query (fun keep -> { keep })
  |+ opt_field "keep" RPC_arg.int (fun t -> t.keep)
  |> seal

let storage_gc =
  RPC_service.get_service
    ~description: "Trigger a context garbage collection, before block"
    ~query: keep_query
    ~output: unit
    RPC_path.(root / "storage" / "context" / "gc")

let storage_revert =
  RPC_service.get_service
    ~description: "Revert IronTez storage back to Irmin"
    ~query: RPC_query.empty
    ~output: unit
    RPC_path.(root / "storage" / "context" / "revert" )
