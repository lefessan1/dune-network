(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type error += Proxy_RPC_Port_already_in_use of P2p_point.Id.t list

(* TODO : Register error *)

let default_proxy_rpc_port = 8765

let resolve_addr ~default_addr ?default_port ?(passive = false) peer =
  let addr, port = P2p_point.Id.parse_addr_port peer in
  let node = if addr = "" || addr = "_" then default_addr else addr
  and service =
    match port, default_port with
    | "", None -> invalid_arg ""
    | "", Some default_port -> string_of_int default_port
    | port, _ -> port in
  Lwt_utils_unix.getaddrinfo ~passive ~node ~service

let monitor_encoding = Tezos_signer_backends_unix.Proxy.monitor_encoding

module S = struct

  let monitor =
    RPC_service.get_service
      ~description:""
      ~query:RPC_query.empty
      ~output: monitor_encoding
      RPC_path.(root / "proxy_monitor")

end

let build_rpc_directory () =

  RPC_directory.register0 RPC_directory.empty S.monitor begin fun () () ->
    Tezos_signer_backends_unix.Proxy.get_status ()
  end

let launch_rpc_server (addr, port) =
  let open Signer_logging in
  let host = Ipaddr.V6.to_string addr in
  let dir = build_rpc_directory () in
  let mode = `TCP (`Port port) in
  lwt_log_notice Tag.DSL.(fun f ->
      f "Starting a RPC server listening on %a:%a"
      -% t event "proxy_rpc_start"
      -% a host_name host
      -% a port_number port) >>= fun () ->
  Lwt.catch begin fun () ->
    RPC_server.launch ~host mode dir
      ~media_types:Media_type.all_media_types
      ~cors:{ allowed_origins = [] ;
              allowed_headers = [] } >>= return
  end begin function
    | Unix.Unix_error(Unix.EADDRINUSE, "bind","") ->
        fail (Proxy_RPC_Port_already_in_use [(addr,port)])
    | exn -> Lwt.return (error_exn exn)
  end

let init_rpc rpc_addr = match rpc_addr with
  | None ->
      return_nil
  | Some addr ->
      resolve_addr ~default_addr:"::" ~default_port:default_proxy_rpc_port ~passive:true addr >>= function
      | [] -> failwith "Cannot resolve listening address: %S" addr
      | addrs ->
          map_s (launch_rpc_server) addrs
