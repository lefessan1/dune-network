(************************************************************************)
(*                           Dune Network                               *)
(*                                                                      *)
(*  Copyright 2019 Origin-Labs                                          *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

type node = {
  state : State.t ;
  p2p: Distributed_db.p2p ;
}

module Encoding = struct
  open Data_encoding

  let version =
    RPC_service.get_service
      ~description:"Return the current version."
      ~query: RPC_query.empty
      ~output:
        (obj4
           (req "commit_date" string)
           (req "build_date" string)
           (req "commit" string)
           (req "branch" string))
      RPC_path.(root / "dune" / "version")

  let banned_points =
    RPC_service.get_service
      ~description:"Return the list of banned points."
      ~query: RPC_query.empty
      ~output:
        (list
           (obj2
              (req "id" P2p_point.Id.encoding)
              (req "info" P2p_point.Info.encoding)))
      RPC_path.(root / "dune" / "banned" / "points")

  let banned_peers =
    RPC_service.get_service
      ~description:"Return the list of banned peers."
      ~query: RPC_query.empty
      ~output:
        (list
           (obj2
              (req "id" P2p_peer.Id.encoding)
              (req "info"
                 (P2p_peer.Info.encoding Peer_metadata.encoding
                    Connection_metadata.encoding))))
      RPC_path.(root / "dune" / "banned" / "peers")


  let lmdb_envinfo_encoding =
    let open Lmdb in
    conv
      ( fun { mapsize ;
              last_pgno ;
              last_txnid ;
              maxreaders ;
              numreaders
            } ->
        ( Int64.of_int mapsize ,
          last_pgno ,
          last_txnid ,
          maxreaders ,
          numreaders ) )
      ( fun ( mapsize ,
              last_pgno ,
              last_txnid ,
              maxreaders ,
              numreaders ) ->
        { mapsize = Int64.to_int mapsize;
          last_pgno ;
          last_txnid ;
          maxreaders ;
          numreaders } )
      (obj5
         (req "mapsize" int64)
         (req "last_pgno" int31)
         (req "last_txnid" int31)
         (req "maxreaders" int31)
         (req "numreaders" int31)
      )

  let lmdb_stat_encoding =
    let open Lmdb in
    conv
      ( fun  {
          psize ;
          depth ;
          branch_pages ;
          leaf_pages ;
          overflow_pages ;
          entries ;
        } ->
          (
            psize ,
            depth ,
            branch_pages ,
            leaf_pages ,
            overflow_pages ,
            entries
          )
      )
      (fun
        (
          psize ,
          depth ,
          branch_pages ,
          leaf_pages ,
          overflow_pages ,
          entries
        ) ->  {
          psize ;
          depth ;
          branch_pages ;
          leaf_pages ;
          overflow_pages ;
          entries ;
        }
      )
      (obj6
         (req "psize" int31)
         (req "depth" int31)
         (req "branch_pages" int31)
         (req "leaf_pages" int31)
         (req "overflow_pages" int31)
         (req "entries" int31)
      )

  let db_stats =
    RPC_service.get_service
      ~description:"Return DB storage information."
      ~query: RPC_query.empty
      ~output:
        (list
           (tup2 string
              (tup2 lmdb_stat_encoding lmdb_envinfo_encoding)))
      RPC_path.(root / "dune" / "db" / "stats")

end

module Handler = struct

  let version _node () () =
    return (Tezos_base.Current_git_info.committer_date,
            Tezos_base.Current_git_info.build_date,
            Tezos_base.Current_git_info.commit_hash,
            Tezos_base.Current_git_info.branch)


  let banned_points node () () =
    return (P2p_directory.banned_points node.p2p)

  let banned_peers node () () =
    return (P2p_directory.banned_peers node.p2p)

  let db_stats _node () () =
    let list = Lmdb.list () in
    let list =
      List.map (
        fun
          ( _maxreaders , _maxdbs , _mapsize ,
            _flags , file_name, _file_perm , t )    ->
          let stats = Lmdb.stat t in
          let info = Lmdb.envinfo t in
          Filename.basename file_name, (stats, info)
      ) list in
    return list

end

let build_rpc_directory ( node : node  ) =

  let dir : unit RPC_directory.t ref = ref RPC_directory.empty in

  let register0 s f =
    dir := RPC_directory.register !dir s (fun () p q -> f p q) in
  (*
  let register1 s f =
    dir := RPC_directory.register !dir s (fun ((), a) p q -> f a p q) in
  let register2 s f =
    dir := RPC_directory.register !dir s (fun (((), a), b) p q -> f a b p q) in
*)

  register0 Encoding.version (Handler.version node);
  register0 Encoding.banned_points (Handler.banned_points node);
  register0 Encoding.banned_peers (Handler.banned_peers node);
  register0 Encoding.db_stats (Handler.db_stats node);
  !dir
