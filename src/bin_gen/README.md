
```
./_obuild/tezos-gen/tezos-gen.asm --dir tz -n 9 --clean
```

will create 9 (as `-n 9) directories `node1` to `node9` in directory
`tz`, containing a configuration for Tezos nodes. Option `--clean` tells
the tool to remove former data (but identities are kept if already
computed). Every node is initialized in alpha protocol, and knows about
other nodes as bootstrap nodes.

You can use `node1/tezos-node.sh` to run the node 1, and
`node1/tezos-client.sh` to connect to it. Nodes have P2P ports in the
range 8731-... and RPC ports in the range 9731-...




