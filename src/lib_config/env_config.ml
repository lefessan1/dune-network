(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

let tezos_compatible_mode =
  match Tezos_stdlib.Environment_variable.(
      get @@
      make "DUNE_TZ_COMPATIBILITY"
        ~description:"(De)Activate Tezos compatibility layer"
        ~allowed_values:[
          "true" ; "y" ; "Y" ; "yes" ; "false" ; "n" ; "N" ; "no" ]
    ) with
  | exception Not_found -> false
  | "true" | "y" | "Y" | "yes" | "" -> true
  | "false" | "n" | "N" | "no" -> false
  | _ -> failwith "bad DUNE_TZ_COMPATIBILITY environment variable"
