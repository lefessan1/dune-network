(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type config = Config_type.config = {
  network : string ;
  forced_protocol_upgrades : (Int32.t * string) list;
  genesis_key : string ;
  genesis_time : string ;
  genesis_block : string ;
  genesis_protocol : string ;
  p2p_version : string ;
  max_operation_data_length : int ;
  encoding_version : int ;
  prefix_dir : string ; (* "dune" or "tezos" *)
  p2p_port : int ; (* 9733 *)
  rpc_port : int ; (* 8733 *)
  discovery_port: int ; (* 10733 *)
  signer_tcp_port: int ; (* 7733 *)
  signer_http_port: int ; (* 6733 *)
  bootstrap_peers  : string list ; (* [ "boot.tzbeta.net" ] *)
  cursym : string ;
  tzcompat : bool ;
  protocol_revision : int ;
}

let () =
  Printexc.register_printer (function
      | Json_encoding.Cannot_destruct (path, exn) ->
          Some (Printf.sprintf "%s (%s)"
                  (String.concat "/"
                     (List.map (function
                            `Field f -> f
                          | `Index n -> string_of_int n
                          | `Next -> "."
                          | `Star -> "*"
                        )
                         path)) (Printexc.to_string exn))
      | _ -> None)

module FILE = struct

  open Json_encoding

  let default = match Set_config.config_option with
    | Some config -> config
    | None -> Set_config_private.config

  let encoding =
    conv
      (fun
        {
          forced_protocol_upgrades ; genesis_key ; p2p_version ;
          max_operation_data_length ; encoding_version ;
          prefix_dir ; p2p_port ; rpc_port ;
          bootstrap_peers ;
          genesis_time ;  genesis_block ; genesis_protocol ;
          network ; cursym ; tzcompat=_ ;
          discovery_port ; signer_tcp_port ; signer_http_port ;
          protocol_revision ;
        }
        ->
          (
            forced_protocol_upgrades , genesis_key , p2p_version ,
            max_operation_data_length , encoding_version ,
            prefix_dir , p2p_port , rpc_port ,
            bootstrap_peers , genesis_time ) ,
          ( genesis_block , genesis_protocol ,
            network , cursym ,
            discovery_port , signer_tcp_port , signer_http_port ,
            protocol_revision
          )
      )
      (fun
        (
          (
            forced_protocol_upgrades , genesis_key , p2p_version ,
            max_operation_data_length , encoding_version ,
            prefix_dir , p2p_port , rpc_port ,
            bootstrap_peers  , genesis_time ) ,
          ( genesis_block , genesis_protocol ,
            network , cursym ,
            discovery_port , signer_tcp_port , signer_http_port,
            protocol_revision
          )
        )
        ->
          {
            forced_protocol_upgrades ; genesis_key ; p2p_version ;
            max_operation_data_length ; encoding_version ;
            prefix_dir ; p2p_port ; rpc_port ;
            bootstrap_peers ;
            genesis_time ;  genesis_block ; genesis_protocol ;
            network ; cursym ; tzcompat=false ;
            discovery_port ; signer_tcp_port ; signer_http_port ;
            protocol_revision ;
          }
      )
      (merge_objs
         (obj10
            (dft "forced_protocol_upgrades"
               (list (tup2 int32 string)) [])
            (req "genesis_key" string)
            (req "p2p_version" string)
            (dft "max_operation_data_length" int default.max_operation_data_length)
            (dft "encoding_version" int default.encoding_version )
            (dft "prefix_dir" string default.prefix_dir)
            (dft "p2p_port" int default.p2p_port)
            (dft "rpc_port" int default.rpc_port)
            (dft "bootstrap_peers" (list string) default.bootstrap_peers)
            (dft "genesis_time" string default.genesis_time)
         )
         (obj8
            (dft "genesis_block" string default.genesis_block)
            (dft "genesis_protocol" string default.genesis_protocol)
            (dft "network" string (default.network ^ "+env"))
            (dft "cursym" string default.cursym)
            (dft "discovery_port" int default.discovery_port)
            (dft "signer_tcp_port" int default.signer_tcp_port)
            (dft "signer_http_port" int default.signer_http_port)
            (dft "protocol_revision" int default.protocol_revision)
         )
      )

  let variable = Tezos_stdlib.Environment_variable.make "DUNE_CONFIG"
      ~description:"Select predefined or custom configuration"
      ~allowed_values:[ "mainnet" ; "testnet" ; "private" ; "<filename.json>" ]

  let read_file filename =
    let ic = open_in filename in
    let b = Buffer.create 100 in
    let len = 100 in
    let s = Bytes.create len in
    let rec iter () =
      let nread = input ic s 0 len in
      if nread > 0 then begin
        Buffer.add_subbytes b s 0 nread;
        iter ()
      end
    in
    iter ();
    close_in ic;
    Buffer.contents b


  let get_config () =
    let config_filename = Tezos_stdlib.Environment_variable.get variable
    in
    Printf.eprintf "%s=%S\n%!" variable.name config_filename;
    match config_filename with
    | "mainnet" -> Set_config_mainnet.config
    | "testnet" -> Set_config_testnet.config
    | "private" -> Set_config_private.config
    | _ ->
        if not (Sys.file_exists config_filename) then begin
          Printf.eprintf "Error: %s targets inexistent file %S.\n%!"
            variable.name config_filename;
          exit 2
        end;
        let config =
          let s = read_file config_filename in
          try
            let json = Ezjsonm.from_string s in
            Json_encoding.destruct encoding json
          with exn ->
            Printf.eprintf "Error: %s in %S\n%!"
              (Printexc.to_string exn) config_filename;
            exit 2
        in
        config

  let config = try Some (get_config ()) with Not_found -> None

end

let config = match FILE.config with
  | Some config -> config
  | None ->
      match Set_config.config_option with
      | Some config -> config
      | None ->
          Printf.eprintf
            "Error: the env variable %s must be defined.\n%!"
            FILE.variable.name;
          exit 2


let config = if Env_config.tezos_compatible_mode then
    { config with
      tzcompat = true ;
      cursym = "\xEA\x9C\xA9" }
  else config

let () =
  if not config.tzcompat then
    Printf.eprintf "Network Config: %s\n%!" config.network;
  ExternalCallback.register "get-config" (fun () -> config)

let config = ExternalCallback.callback "get-config" ()
