(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context
module Repr = Lang1_script_repr

open Repr

type execution_result = {
  ctxt : context ;
  storage : const ;
  result : const ;
  operations : packed_internal_operation list ;
}

type fee_execution_result = {
  ctxt : context ;
  max_fee : Tez.t ;
  max_storage : Z.t ;
}

let interp ctxt _mode ~source:_ ~payer:_ ~self:_ state instr = match instr with
  | NOOP -> return (ctxt, state)

let execute ctxt mode ~source ~payer ~self ~code ~storage ~parameter
    ~collect_call:_ ~amount:_ ~apply:_ =
  interp ctxt mode ~source ~payer ~self (parameter, storage) code
  >>=? fun (ctxt, (_result_state, _storage)) ->
(*  Dune_storage.update_contract_storage ctxt self
    (Lang1_repr.Const storage) >>=? fun ctxt -> *)
  return { ctxt ; storage; result = VOID ; operations = [] }

let typecheck ctxt ~code ~storage =
  return ((code, storage, None), ctxt)

let execute_fee_script ctxt ~source ~payer ~self ~fee_code ~storage ~parameter ~amount:_ =
  interp ctxt Script_ir_translator.Readable ~source ~payer ~self (parameter, storage) fee_code
  >>=? fun (ctxt, _) ->
  return { ctxt ; max_fee = Tez.zero ; max_storage = Z.zero }

(* Disable after revision 1 *)

let typecheck ctxt ~code ~storage =
  if Dune_storage.has_protocol_revision ctxt 1 then
    failwith "No Lang1"
  else
    typecheck ctxt ~code ~storage

let execute ctxt mode ~source ~payer ~self ~code ~storage ~parameter
    ~collect_call ~amount ~apply =
  if Dune_storage.has_protocol_revision ctxt 1 then
    failwith "No Lang1"
  else
    execute ctxt mode ~source ~payer ~self ~code ~storage ~parameter
      ~collect_call ~amount ~apply

let execute_fee_script ctxt ~source ~payer ~self ~fee_code ~storage ~parameter ~amount =
  if Dune_storage.has_protocol_revision ctxt 1 then
    failwith "No Lang1"
  else
    execute_fee_script ctxt ~source ~payer ~self ~fee_code ~storage ~parameter ~amount
