(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Dune_script_sig

include Dune_script_registration

type error += Lazy_dune_script_decode

let serialized_cost bytes =
  let open Gas_limit_repr in
  alloc_mbytes_cost (MBytes.length bytes)

let minimal_deserialize_cost lexpr =
  Data_encoding.apply_lazy
    ~fun_value:(fun _ -> Gas_limit_repr.free)
    ~fun_bytes:(fun b -> serialized_cost b)
    ~fun_combine:(fun c_free _ -> c_free)
    lexpr

let () =
  Lang1_repr.init ();
  Dune_script_registration.close_registration ()

let lang_encoding =
  let open Data_encoding in
  Data_encoding.union ~tag_size:`Uint8 @@
  List.map (fun (_name, m) ->
      let module Lang = (val m : S) in
      case (Tag Lang.tag)
        ~title:(String.capitalize_ascii Lang.lang_str)
        Lang.lang_encoding
        (fun k -> Some k)
        (fun k -> k)
    ) (MapLang.bindings !languages)

let const_encoding =
  let open Data_encoding in
  union ~tag_size:`Uint8 @@
  List.map (fun (_name, m) ->
      let module Lang = (val m : S) in
      let title =
        String.capitalize_ascii
          (String.concat "." [ Lang.lang_str ; "const" ]) in
      case (Tag Lang.tag) ~title
        Lang.const_encoding
        Lang.is_const
        Lang.const
    ) (MapLang.bindings !languages)

let code_encoding =
  let open Data_encoding in
  union ~tag_size:`Uint8 @@
  List.map (fun (_name, m) ->
      let module Lang = (val m : S) in
      let title =
        String.capitalize_ascii
          (String.concat "." [ Lang.lang_str ; "code" ]) in
      case (Tag Lang.tag) ~title
        Lang.code_encoding
        Lang.is_code
        Lang.code
    ) (MapLang.bindings !languages)

type lazy_const = const Data_encoding.lazy_t

type lazy_code = code Data_encoding.lazy_t

let lazy_const_encoding =
  Data_encoding.lazy_encoding const_encoding

let lazy_const const =
  Data_encoding.make_lazy const_encoding const

let lazy_code_encoding =
  Data_encoding.lazy_encoding code_encoding

let lazy_code code =
  Data_encoding.make_lazy code_encoding code

let get_module lang =
  match MapLang.find_opt lang !languages with
  | None ->
      let { Dune_script_sig.name = n; version = v, x } = lang in
      failwith (Format.sprintf "Unknown language %s.%d.%d" n v x)
  | Some language -> language

let module_of_const c =
  try lang_of_const c
  with Not_found -> failwith "Unknown language"

let module_of_code c =
  try lang_of_code c
  with Not_found -> failwith "Unknown language"


let unit lang =
  let module Lang = (val (get_module lang) : S) in
  Lang.Const Lang.unit

let get_unit code =
  let module Lang = (val (module_of_code code) : S) in
  Lang.Const Lang.unit

let is_unit c =
  let module Lang = (val (module_of_const c) : S) in
  match c with
  | Lang.Const c -> Lang.is_unit c
  | _ -> assert false

let pp_const ppf c =
  let module Lang = (val (module_of_const c) : S) in
  match c with
  | Lang.Const c -> Lang.pp_const ppf c
  | _ -> assert false

let deserialized_const_cost c =
  let module Lang = (val (module_of_const c) : S) in
  match c with
  | Lang.Const c -> Lang.deserialized_const_cost c
  | _ -> assert false

let deserialized_code_cost c =
  let module Lang = (val (module_of_code c) : S) in
  match c with
  | Lang.Code c -> Lang.deserialized_code_cost c
  | _ -> assert false

let traversal_const_cost c =
  let module Lang = (val (module_of_const c) : S) in
  match c with
  | Lang.Const c -> Lang.traversal_const_cost c
  | _ -> assert false

let traversal_code_cost c =
  let module Lang = (val (module_of_code c) : S) in
  match c with
  | Lang.Code c -> Lang.traversal_code_cost c
  | _ -> assert false

let force_decode deserialized_cost lexpr =
  let account_deserialization_cost =
    Data_encoding.apply_lazy
      ~fun_value:(fun _ -> false)
      ~fun_bytes:(fun _ -> true)
      ~fun_combine:(fun _ _ -> false)
      lexpr in
  match Data_encoding.force_decode lexpr with
  | Some v ->
      if account_deserialization_cost then
        ok (v, deserialized_cost v)
      else
        ok (v, Gas_limit_repr.free)
  | None -> error Lazy_dune_script_decode

let force_bytes traversal_cost expr =
  let open Gas_limit_repr in
  let account_serialization_cost =
    Data_encoding.apply_lazy
      ~fun_value:(fun v -> Some v)
      ~fun_bytes:(fun _ -> None)
      ~fun_combine:(fun _ _ -> None)
      expr in
  match Data_encoding.force_bytes expr with
  | bytes ->
      begin match account_serialization_cost with
        | Some v -> ok (bytes, traversal_cost v +@ serialized_cost bytes)
        | None -> ok (bytes, Gas_limit_repr.free)
      end
  | exception _ -> error Lazy_dune_script_decode

let force_decode_const = force_decode deserialized_const_cost
let force_decode_code = force_decode deserialized_code_cost
let force_bytes_const = force_bytes traversal_const_cost
let force_bytes_code = force_bytes traversal_code_cost
