(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

let lang = {
  Dune_script_sig.name = "lang1" ;
  version = 0, 1 ;
}

type const = VOID

type code = NOOP

type location = int

let unit = VOID

let is_unit = function VOID -> true

let const_encoding =
  let open Data_encoding in
  conv (fun VOID -> ()) (fun () -> VOID) (constant "VOID")
  (* string_enum [ "VOID", VOID ] *)

let code_encoding =
  let open Data_encoding in
  conv (fun NOOP -> ()) (fun () -> NOOP) (constant "NOOP")
  (* string_enum [ "NOOP", NOOP ] *)

let location_encoding =
  Data_encoding.unit

let deserialized_const_cost = function
  | VOID -> Gas_limit_repr.alloc_cost 1

let deserialized_code_cost = function
  | NOOP -> Gas_limit_repr.alloc_cost 1

let traversal_const_cost = function
  | VOID -> Gas_limit_repr.free

let traversal_code_cost = function
  | NOOP -> Gas_limit_repr.free

let pp_const ppf VOID =
  Format.fprintf ppf "VOID"
