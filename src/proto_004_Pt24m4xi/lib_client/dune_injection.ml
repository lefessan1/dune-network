(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context
open Apply_results

type 'kind forge_result =
  MBytes.t * 'kind contents_result option

let confirm (cctxt: #Client_context.io) ~msg ~prompt =
  cctxt#prompt "%s [y/N]: " prompt >>=? function
  | "Y" | "y" | "yes" | "Yes" ->
      cctxt#message "Yes" >>= return
  | _ ->
      cctxt#message "No" >>= fun () ->
      failwith "%s" msg

let rec extract_contents_result :
  type kind. kind contents_and_result_list -> kind contents_result_list
  = function
    | Single_and_result (_, cr) -> Single_result cr
    | Cons_and_result (_, cr, rest) -> Cons_result (cr, extract_contents_result rest)

let inject_batch_manager_operations
    cctxt ~chain ~block ?branch ?confirmations ?dry_run ?verbose_signing
    ~source ~src_pk ~src_sk ?fee ?(gas_limit = Z.minus_one)
    ?(storage_limit = (Z.of_int (-1))) ?counter ~fee_parameter
    (operations : packed_manager_operation list) =
  begin
    match counter with
    | None ->
        Alpha_services.Contract.counter
          cctxt (chain, block) source >>=? fun pcounter ->
        let counter = Z.succ pcounter in
        return counter
    | Some counter ->
        return counter
  end >>=? fun counter ->
  Alpha_services.Contract.manager_key
    cctxt (chain, block) source >>=? fun (_, key) ->
  let is_reveal : type kind. kind manager_operation -> bool = function
    | Reveal _ -> true
    | _ -> false in
  let compute_fee, fee =
    match fee with
    | None -> true, Tez.zero
    | Some fee -> false, fee in
  let contents_of_manager_operation counter
      (type kind) (operation : kind manager_operation)
    : kind Kind.manager contents = match operation with
    | Reveal _ as operation ->
        Manager_operation { source ; fee = Tez.zero ; counter ;
                            gas_limit = Z.of_int 10_000 ;
                            storage_limit = Z.zero ;
                            operation }
    | operation ->
        Manager_operation { source ; fee ; counter ;
                            gas_limit ; storage_limit ; operation } in
  let rec make_operation_list counter = function
    | [] -> [], Z.pred counter
    | [Manager operation] ->
        [Contents (contents_of_manager_operation counter operation)], counter
    | Manager operation :: operations ->
        let operation_list, last_counter =
          make_operation_list (Z.succ counter) operations in
        (Contents (contents_of_manager_operation counter operation) ::
         operation_list), last_counter in
  let operations, added_reveal = match key, operations with
    | None, Manager (Transaction {
        amount ; parameters ; destination ;
        collect_call = Some { fee_gas ; _ }
      }) :: operations ->
        let first = Manager (Transaction {
            amount ; parameters ; destination ;
            collect_call = Some { fee_gas ; reveal_pubkey = Some src_pk }
          }) in
        first :: operations, false
    | None, Manager operation :: _ when not (is_reveal operation) ->
        Manager (Reveal src_pk) :: operations, true
    | _ -> operations, false in
  let operation_list, last_counter = make_operation_list counter operations in
  let Contents_list contents = Operation.of_list operation_list in
  Injection.inject_operation cctxt ~chain ~block ?confirmations ?dry_run
    ~fee_parameter ~compute_fee
    ?verbose_signing ?branch ~src_sk contents >>=? fun (oph, op, result) ->
  let packed_results = match pack_contents_list op result with
    | Cons_and_result (_, _, rest) when added_reveal ->
        Contents_result_list (extract_contents_result rest)
    | pcr ->
        Contents_result_list (extract_contents_result pcr) in
  return (oph, to_list packed_results, last_counter)

let forge_operation
    (type kind) cctxt ~chain ~block
    ?branch
    ~fee_parameter
    ?compute_fee
    ?forge_file
    (contents: kind contents_list) =
  Tezos_client_base.Client_confirmations.wait_for_bootstrapped cctxt >>=? fun () ->
  Injection.may_patch_limits
    cctxt ~chain ~block ?branch
    ~fee_parameter
    ?compute_fee
    contents >>=? fun contents ->
  let confirm_fail errs =
    cctxt#warning "Error: %a"
      (Michelson_v1_error_reporter.report_errors
         ~details:false
         ~show_source:false
         ?parsed:None) errs >>= fun () ->
    confirm cctxt
      ~prompt:"Simulation failed, would you still like \
               to forge the operation?"
      ~msg:"Forging was rejected by the user." >>=? fun () ->
    return_none
  in
  Injection.simulate cctxt ~chain ~block ?branch contents >>= begin function
    | Error errs -> confirm_fail errs
    | Ok (_, op, result) ->
        cctxt#message
          "@[<v 2>Simulation result:@,%a@]"
          Operation_result.pp_operation_result
          (op.protocol_data.contents, result.contents) >>= fun () ->
        match Injection.detect_script_failure result with
        | Error errs -> confirm_fail errs
        | Ok () -> return_some result.contents
  end >>=? fun result ->
  Injection.get_branch cctxt ~chain ~block branch >>=? fun (_chain_id, branch) ->
  let bytes =
    Data_encoding.Binary.to_bytes_exn
      Operation.unsigned_encoding
      ({ branch }, Contents_list contents) in
  (match forge_file with
   | None -> cctxt#message "@[<v>Serialized unsigned operation:@,%a@]" MBytes.pp_hex bytes
   | Some f ->
       let `Hex hex = MBytes.to_hex bytes in
       let oc = open_out f in
       output_string oc hex;
       close_out oc;
       cctxt#message "@[<v>Serialized unsigned operation written to %s@]" f
  ) >>= fun () ->
  return (bytes, result)

let forge_manager_operation
    cctxt ~chain ~block ?branch
    ~source ~src_pk ?fee ?gas_limit
    ?storage_limit ?counter ~fee_parameter
    ?forge_file
    (type kind) (operation : kind manager_operation) =
  Injection.add_reveal_manager_operation cctxt ~chain ~block
    ~source ~src_pk ?fee
    ?gas_limit
    ?storage_limit
    ?counter
    operation >>=? fun (reveal_operation, manager_operation) ->
  let compute_fee = match fee with None -> true | Some _ -> false in
  match reveal_operation with
  | Some reveal_operation -> begin
      let contents = Cons (reveal_operation, Single manager_operation) in
      forge_operation cctxt ~chain ~block
        ~fee_parameter ~compute_fee
        ?branch ?forge_file contents >>=? function
      | bytes, None -> return (bytes, None)
      | bytes, Some (Cons_result (_, Single_result result)) ->
          return (bytes, Some result)
      | _ -> assert false (* Grrr... *)
    end
  | None ->
      let contents = Single manager_operation in
      forge_operation cctxt ~chain ~block
        ~compute_fee ~fee_parameter ?branch ?forge_file contents >>=? function
      | bytes, None -> return (bytes, None)
      | bytes, Some (Single_result result) ->
          return (bytes, Some result)
      | _ -> assert false (* Grrr... *)
