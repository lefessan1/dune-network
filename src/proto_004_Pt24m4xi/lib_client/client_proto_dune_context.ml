(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context
open Tezos_micheline
open Dune_lang (* Michelson_v1_* = Dune_lang_v1_* *)
open Client_proto_context

(* `Program_or_hash.source_param` can represent an argument that
   is either a program, a contract (its code) or a code hash.
*)

type program =
  | Program of
      Michelson_v1_parser.parsed Micheline_parser.parsing_result
  | Program_hash of Script_expr_hash.t
  | Contract_hash of Contract.t

module Program_or_hash = struct

  open Clic
  open Client_aliases.Ambiguity

  let autocomplete cctxt =
    Client_proto_programs.Program.autocomplete cctxt >>=? fun scripts ->
    Client_proto_contracts.ContractAlias.autocomplete cctxt
    >>=? fun contracts ->
    return ( scripts @ contracts )

  let find_program cctxt s =
    Client_proto_programs.Program.parse_source_string_ambiguous cctxt s
    >>= fun (programs, errs1) ->
    Lwt.return ( List.map (fun v -> Program v ) programs, errs1 )

  let find_contract cctxt s =
    Client_proto_contracts.ContractAlias.get_contract cctxt s
    >>= fun (contracts, errs2) ->
    Lwt.return ( List.map (fun v -> Contract_hash v ) contracts, errs2 )

  let find_hash s =
    match Script_expr_hash.of_b58check_opt s with
    | Some hash -> Lwt.return ( [ Program_hash hash ], [] )
    | None -> Lwt.return ( [] , [] )

  let parse_source_string_ambiguous cctxt s =
    match String.split ~limit:1 ':' s with
    | [ ( "file" | "text" | "alias" ) ; name ] -> find_program cctxt name
    | [ ( "key" | "contract" | "chash" ) ; name ] -> find_contract cctxt name
    | [ "phash" ; name ] -> find_hash name
    | _ ->
        find_program cctxt s >>= fun program ->
        find_contract cctxt s >>= fun contract ->
        find_hash s >>= fun hash ->
        lists_combine contract hash >>=
        lists_combine program

  let source_param
      ?(name="prg")
      ?(desc="script expression or code hash or contract")
      next
    =
    param ~name ~desc
      (parameter ~autocomplete
         (fun cctxt s ->
            parse_source_string_ambiguous cctxt s >>=
            check_ambiguity
              ~kind:"program"
              ~names:[ "script expression" ; "code hash"; "contract" ]
              ~prefixes: [ "alias:" ; "text:" ; "file:" ;
                           "key:" ; "contract:" ; "chash:";
                           "phash:" ]
              s >>=? fun x -> return (snd x)
         ))
      next
end

module Ed25519_public_key_hash = struct

  let param
      ?(name="pkh")
      ?(desc="public key hash or alias")
      next
    =
    Clic.param ~name ~desc
      (Clic.parameter ~autocomplete:Client_keys.Public_key_hash.autocomplete
         (fun _cctxt s ->
            match Ed25519.Public_key_hash.of_b58check_opt s with
            | Some hash -> return hash
            | None -> failwith "not a dn1/tz1 address"
         ))
      next

end

let parse_expression arg =
  Lwt.return
    (Micheline_parser.no_parsing_error
       (Michelson_v1_parser.parse_expression arg))

let batch_transfer (cctxt : #Alpha_client_context.full)
    ~chain ~block ?confirmations
    ?dry_run ?verbose_signing
    ?branch ~source ~src_pk ~src_sk ~destinations ~total
    ?fee ?gas_limit ?storage_limit ?counter ?batch_size
    ~fee_parameter
    () =
  let add_to_current_batch op = function
    | [] -> [[op]]
    | batch :: rest -> (op :: batch) :: rest in
  let add_to_new_batch op = function
    | [] -> [[op]]
    | batch :: rest -> [op] :: List.rev batch :: rest in
  let reorder_batches = function
    | [] -> []
    | batch :: rest -> List.rev (List.rev batch :: rest) in
  fold_left_s
    (fun (operations, total, current_batch_size) (destination, amount, fee_gas, arg) ->
       begin match arg with
         | Some arg ->
             parse_expression arg >>=? fun { expanded = arg ; _ } ->
             return_some arg
         | None -> return_none
       end >>=? fun parameters ->
       let parameters = Option.map ~f:Script.lazy_expr parameters in
       let collect_call = match fee_gas with
         | Some fee_gas -> Some { fee_gas ; reveal_pubkey = None }
         | None -> None in
       let op = Manager (Transaction { amount ; parameters ; destination ; collect_call }) in
       Lwt.return (Environment.wrap_error Tez.( amount +? total )) >>|? fun total ->
       let operations, current_batch_size = match batch_size with
         | Some batch_size when current_batch_size >= batch_size ->
             add_to_new_batch op operations, 1
         | _ -> add_to_current_batch op operations, current_batch_size + 1 in
       operations, total, current_batch_size
    ) ([], Tez.zero, 0) destinations >>=? fun (operations, ops_total, _) ->
  fail_unless Tez.( total = ops_total )
    (failure "@[<v 2>Total doesn't match:@ \
              Expected: %a@ \
              Got: %a @]"
       Tez.pp total Tez.pp ops_total) >>=? fun () ->
  let operations = reorder_batches operations in
  fold_left_s (fun (ophs, results, counter) batch ->
      Dune_injection.inject_batch_manager_operations
        cctxt ~chain ~block ~confirmations:0 (* Wait after *)
        ?dry_run ?verbose_signing
        ?branch ~source ?fee ?gas_limit ?storage_limit ?counter
        ~src_pk ~src_sk
        ~fee_parameter
        batch >>|? fun (oph, result, last_counter) ->
      oph :: ophs, result :: results, Some (Z.succ last_counter)
    ) ([], [], counter) operations >>=? fun (ophs, results, _counter) ->
  let ophs = List.rev ophs in
  let result = List.fold_left (fun acc r -> List.rev_append r acc) [] results in
  begin match confirmations, dry_run with
    | None, _ | _, Some true -> return_unit
    | Some confirmations, _ ->
        cctxt#message
          "@[<v 2>Waiting for the following operations to be included...@,%a@]"
          (Format.pp_print_list Operation_hash.pp) ophs
        >>= fun () ->
        iter_p (fun oph ->
            Client_confirmations.wait_for_operation_inclusion cctxt
              ~predecessors:(10 + List.length ophs)
              ~chain (* ?branch *) ~confirmations oph >>=? fun _ ->
            return_unit
          ) ophs
  end >>=? fun () ->
  let Contents_result_list result_list = Apply_results.of_list result in
  Lwt.return
    (Injection.originated_contracts result_list) >>=? fun contracts ->
  return (result, contracts)


let activate_protocol_operation (cctxt : #Alpha_client_context.full)
    ~chain ~block ?confirmations
    ?dry_run ?verbose_signing
    ?branch ~source ~src_pk ~src_sk

    ~level
    ?protocol
    ?protocol_parameters (* operation specifics *)

    ?fee ?gas_limit ?storage_limit ?counter
    ~fee_parameter
    () =
  let contents = Dune_manager_operation
      ( Dune_activate_protocol { level ; protocol ; protocol_parameters } )
  in
  Injection.inject_manager_operation
    cctxt ~chain ~block ?confirmations
    ?dry_run ?verbose_signing
    ?branch ~source ?fee ?gas_limit ?storage_limit ?counter
    ~src_pk ~src_sk
    ~fee_parameter
    contents >>=? fun (_oph, _op, result as res) ->
  Lwt.return
    (Injection.originated_contracts (Single_result result)) >>=?
  fun contracts ->
  return (res, contracts)

let manage_accounts_operation (cctxt : #Alpha_client_context.full)
    ~chain ~block ?confirmations
    ?dry_run ?verbose_signing
    ?branch ~source ~src_pk ~src_sk

    ~protocol_parameters (* operation specifics *)

    ?fee ?gas_limit ?storage_limit ?counter
    ~fee_parameter
    () =
  let contents = Dune_manager_operation
      ( Dune_manage_accounts protocol_parameters )
  in
  Injection.inject_manager_operation
    cctxt ~chain ~block ?confirmations
    ?dry_run ?verbose_signing
    ?branch ~source ?fee ?gas_limit ?storage_limit ?counter
    ~src_pk ~src_sk
    ~fee_parameter
    contents >>=? fun (_oph, _op, result as res) ->
  Lwt.return
    (Injection.originated_contracts (Single_result result)) >>=?
  fun contracts ->
  return (res, contracts)


let forge_transfer (cctxt : #Alpha_client_context.full)
    ~chain ~block
    ?branch ~source ~src_pk ~destination ?arg
    ~amount ?fee ?gas_limit ?storage_limit ?counter
    ~fee_parameter ?collect_call ?forge_file
    () =
  build_transfer ~destination ?arg ~amount ?collect_call () >>=? fun contents ->
  Dune_injection.forge_manager_operation
    cctxt ~chain ~block
    ?branch ~source ~src_pk ?fee ?gas_limit ?storage_limit ?counter
    ~fee_parameter ?forge_file
    contents

let forge_reveal cctxt
    ~chain ~block
    ?branch ~source ~src_pk ?fee
    ~fee_parameter ?forge_file
    () =
  let compute_fee = match fee with
    | None -> true
    | Some _ -> false in
  build_reveal cctxt ~chain ~block ~source ~src_pk ?fee () >>=? fun contents ->
  Dune_injection.forge_operation cctxt ~chain ~block
    ?branch ~compute_fee ~fee_parameter ?forge_file contents >>=? function
  | bytes, None -> return (bytes, None)
  | bytes, Some (Single_result result) -> return (bytes, Some result)

let forge_originate
    cctxt ~chain ~block
    ?branch ~source ~src_pk ?fee
    ?gas_limit ?storage_limit
    ~fee_parameter ?forge_file
    contents =
  Dune_injection.forge_manager_operation
    cctxt ~chain ~block
    ?branch ~source ?fee ?gas_limit ?storage_limit
    ~src_pk
    ~fee_parameter ?forge_file
    contents >>=? function
  | bytes, None ->
      cctxt#warning "Simulation for origination failed, \
                     no contracts will be saved." >>= fun () ->
      return (bytes, None)
  | bytes, Some result ->
      Lwt.return
        (Injection.originated_contracts (Single_result result)) >>=? function
      | [ contract ] -> return (bytes, Some contract)
      | contracts ->
          failwith
            "The origination introduced %d contracts instead of one."
            (List.length contracts)

let forge_originate_account
    cctxt ~chain ~block
    ?branch ~source ~src_pk ~manager_pkh
    ?(delegatable = false) ?delegate ~balance ?fee
    ~fee_parameter ?forge_file
    () =
  let origination =
    Origination { manager = manager_pkh ;
                  delegate ;
                  script = Noscript ;
                  spendable = true ;
                  delegatable ;
                  credit = balance ;
                  preorigination = None } in
  forge_originate
    cctxt ~chain ~block
    ?branch ~source ~src_pk ?fee
    ~fee_parameter ?forge_file
    origination

let forge_delegate_contract cctxt
    ~chain ~block ?branch
    ~source ~src_pk
    ?fee
    ~fee_parameter ?forge_file
    delegate_opt =
  let operation = Delegation delegate_opt in
  Dune_injection.forge_manager_operation
    cctxt ~chain ~block
    ?branch ~source ~src_pk ?fee ~storage_limit:Z.zero
    ~fee_parameter ?forge_file
    operation

let forge_set_delegate
    cctxt ~chain ~block
    ?fee contract ~src_pk
    ~fee_parameter ?forge_file
    opt_delegate =
  forge_delegate_contract
    cctxt ~chain ~block
    ~source:contract ~src_pk ?fee
    ~fee_parameter ?forge_file
    opt_delegate

let forge_register_as_delegate
    cctxt ~chain ~block
    ?fee
    ~fee_parameter ?forge_file
    src_pk =
  let source = Signature.Public_key.hash src_pk in
  forge_delegate_contract
    cctxt ~chain ~block
    ~source:(Contract.implicit_contract source) ~src_pk ?fee
    ~fee_parameter ?forge_file
    (Some source)

let forge_originate_contract
    (cctxt : #Alpha_client_context.full)
    ~chain ~block
    ?branch
    ?fee
    ?gas_limit
    ?storage_limit
    ~delegate
    ?(delegatable=true)
    ?(spendable=false)
    ~initial_storage
    ~manager
    ~balance
    ~source
    ~src_pk
    ?code_hash
    ?code
    ~fee_parameter
    ?forge_file
    () =
  build_originate_contract
    cctxt
    ~chain ~block
    ~delegate
    ~delegatable
    ~spendable
    ~initial_storage
    ~manager
    ~balance
    ?code_hash
    ?code
    () >>=? fun origination ->
  forge_originate
    cctxt ~chain ~block
    ?branch ~source ~src_pk ?fee ?gas_limit ?storage_limit
    ~fee_parameter ?forge_file
    origination
