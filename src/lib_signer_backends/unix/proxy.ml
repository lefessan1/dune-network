(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Client_keys
open Signer_messages
open Proxy_logging

let wait_r = ref false
let set_wait b = wait_r := b
let request_timeout = 30.

type error += No_working_reverse_signer_connection
(* type error += Remote_timed_out *)

let tcp_scheme = "tcp-proxy"

type connector = {
  conn_time : float ;
  conn_death_time : float option ;
  conn_errors : int ;
  conn : Lwt_unix.file_descr;
  conn_str : string ;
  authorized_keys : Authorized_keys.Response.t
}

type req_status = {
  req_send_time : float ;
  req_pkh : Signature.public_key_hash ;
  req_conn : string option ;
  req_resp_time : float option ;
  req_success : bool option ;
}

let monitor_encoding =
  let open Data_encoding in
  let connector_encoding =
    conv
      (fun { conn_time ; conn_death_time ; conn_errors ; conn_str ; _ } ->
         let time = Option.unopt ~default:Ptime.min @@ Ptime.of_float_s conn_time in
         let death_time = match conn_death_time with
           | None -> None
           | Some f -> Some (Option.unopt ~default:Ptime.min @@ Ptime.of_float_s f) in
         (time, death_time, Int64.of_int conn_errors, conn_str))
      (fun _ -> assert false)
      (obj4
         (req "connect_time" Time.System.encoding)
         (opt "death_time" Time.System.encoding)
         (req "errors" int64)
         (req "addr" string))
  in
  let req_encoding =
    conv
      (fun { req_send_time ; req_pkh ; req_conn ; req_resp_time ; req_success } ->
         let time = Option.unopt ~default:Ptime.min @@ Ptime.of_float_s req_send_time in
         let resp_time = match req_resp_time with
           | None -> Ptime.min
           | Some f -> Option.unopt ~default:Ptime.min @@ Ptime.of_float_s f in
         let success = Option.unopt ~default:false req_success in
         (time, req_pkh, req_conn,
          resp_time, success))
      (fun _ -> assert false)
      (obj5
         (req "send_time" Time.System.encoding)
         (req "pkh" Signature.Public_key_hash.encoding)
         (opt "conn" string)
         (req "resp_time" Time.System.encoding)
         (req "res_ok" bool))
  in
  conv
    (fun (req, alive, dead) -> (req, alive, dead))
    (fun (req, alive, dead) -> (req, alive, dead))
    (obj3
       (req "requests" (list req_encoding))
       (req "alive_conn" (list connector_encoding))
       (req "dead_conn" (list connector_encoding)))

let glob_cond = Lwt_condition.create ()
let glob_c : connector list ref = ref []
let dead_c : connector list ref = ref []

let last_requests = ref []
let mon_req_number = 1000
let mutex = Lwt_mutex.create ()

let get_status () =
  return (!last_requests, !glob_c, !dead_c)

let mk_req_status pkh conn =
  { req_send_time = Unix.gettimeofday () ;
    req_pkh = pkh ;
    req_conn = Some conn.conn_str ;
    req_resp_time = None ;
    req_success = None }

let incr_error_count ?(conn_max_errors = 5) str = match str with
  | None -> ()
  | Some str ->
      try
        let c = List.find (fun conn -> conn.conn_str = str) !glob_c in
        let c = { c with conn_errors = c.conn_errors + 1 } in
        let old_gc = List.filter (fun conn -> not (conn.conn_time = c.conn_time)) !glob_c in
        let gc, dc =
          if c.conn_errors = conn_max_errors then
            let c = { c with conn_death_time = Some (Unix.gettimeofday ()) } in
            old_gc, c :: !dead_c
          else
            c :: old_gc, !dead_c in
        glob_c := gc ;
        dead_c := dc ;
        (* Try to close the remote signer connection in order
           to have it restarts the connection process *)
        ignore @@ Lwt_utils_unix.safe_close c.conn
      with Not_found -> ()

let add_req_status_ok st  =
  let st =
    { st with
      req_resp_time = Some (Unix.gettimeofday ()) ;
      req_success = Some true } in
  let lr =
    if List.length !last_requests = mon_req_number then
      List.rev @@ List.tl @@ List.rev !last_requests
    else !last_requests in
  Lwt_mutex.lock mutex >>= fun () ->
  last_requests := st :: lr ;
  Lwt.return @@ Lwt_mutex.unlock mutex

let add_req_status_err st =
  let st =
    { st with
      req_resp_time = Some (Unix.gettimeofday ()) ;
      req_success = Some false } in
  let lr =
    if List.length !last_requests = mon_req_number then
      List.rev @@ List.tl @@ List.rev !last_requests
    else !last_requests in
  Lwt_mutex.lock mutex >>= fun () ->
  incr_error_count st.req_conn ;
  last_requests := st :: lr ;
  Lwt.return @@ Lwt_mutex.unlock mutex

let add_req_failed pkh =
  let st =
    { req_send_time = Unix.gettimeofday () ;
      req_pkh = pkh ;
      req_conn = None ;
      req_resp_time = None ;
      req_success = None } in
  let lr =
    if List.length !last_requests = mon_req_number then
      List.rev @@ List.tl @@ List.rev !last_requests
    else !last_requests in
  Lwt_mutex.lock mutex >>= fun () ->
  last_requests := st :: lr ;
  Lwt.return @@ Lwt_mutex.unlock mutex

let glob_add c =
  Lwt_mutex.lock mutex >>= fun () ->
  glob_c := c :: !glob_c;
  Lwt_condition.signal glob_cond () ;
  Lwt.return @@ Lwt_mutex.unlock mutex

let rec with_glob f pkh =
  match !glob_c with
  | [] ->
      if !wait_r
      then Lwt_condition.wait glob_cond >>= fun () -> with_glob f pkh
      else
        begin
          add_req_failed pkh >>= fun () ->
          fail No_working_reverse_signer_connection
        end
  | [c] ->
      lwt_log_notice Tag.DSL.(fun f ->
          f "Bouncing request to 1 remote"
          -% t event "bouncing_req") >>= fun () ->
      f c pkh
  | _ :: _ ->
      begin
        lwt_log_notice Tag.DSL.(fun f ->
            f "Bouncing request to %d remotes"
            -% t event "bouncing_req"
            -% s nb_connected_remotes (List.length !glob_c)) >>= fun () ->
        let threads =
          List.map (fun c -> f c pkh) !glob_c in
        let rec choose_no_error = function
          | [x] -> x
          | [] -> fail No_working_reverse_signer_connection
          | l -> Lwt.nchoose_split l >>= (function
              | [], _ -> assert false
              | over, remaining -> begin
                  match
                    List.find_opt (fun res -> match res with
                        | Ok _ -> true | Error _ -> false) over, remaining with
                  | Some x, _ -> Lwt.return x
                  | None, [] ->
                      begin
                        match over with
                        | hd :: _ -> Lwt.return hd
                        | [] -> failwith "No response received from any remotes"
                      end
                  | None, _ -> choose_no_error remaining
                end
            )
        in choose_no_error threads
      end

let init_connection conn =
  Lwt_utils_unix.Socket.send
    conn Request.encoding Request.Authorized_keys >>=? fun () ->
  Lwt_utils_unix.Socket.recv conn
    (result_encoding Authorized_keys.Response.encoding) >>=? fun authorized_keys ->
  Lwt.return authorized_keys >>=? fun authorized_keys ->
  let conn_time = Unix.gettimeofday () in
  let conn_errors = 0 in
  let conn_str = Proxy_logging.string_of_peer conn in
  let conn_death_time = None in
  glob_add
    { conn_time; conn_death_time; conn_errors; conn; conn_str ; authorized_keys; }
  >>= fun () -> Lwt_condition.signal glob_cond ();
  return_unit

module Make(P : sig
    val authenticate: Signature.Public_key_hash.t list -> MBytes.t -> Signature.t tzresult Lwt.t
  end) = struct

  type request_type =
    | Sign_request
    | Deterministic_nonce_request
    | Deterministic_nonce_hash_request

  let build_request pkh data signature = function
    | Sign_request ->
        Request.Sign { Sign.Request.pkh ; data ; signature }
    | Deterministic_nonce_request ->
        Request.Deterministic_nonce
          { Deterministic_nonce.Request.pkh ; data ; signature }
    | Deterministic_nonce_hash_request ->
        Request.Deterministic_nonce_hash
          { Deterministic_nonce_hash.Request.pkh ; data ; signature }

  let signer_operation conn pkh msg request_type =
    begin
      begin match conn.authorized_keys with
        | No_authentication -> return_none
        | Authorized_keys authorized_keys ->
            P.authenticate authorized_keys
              (Sign.Request.to_sign ~pkh ~data:msg) >>=? fun signature ->
            return_some signature
      end
    end >>=? fun signature ->
    let req = build_request pkh msg signature request_type in
    Lwt_utils_unix.Socket.send_protected conn.conn Request.encoding req

  let sign c ?watermark pkh msg =
    let msg =
      match watermark with
      | None -> msg
      | Some watermark ->
          MBytes.concat "" [ Signature.bytes_of_watermark watermark ; msg ] in
    let st = mk_req_status pkh c in
    signer_operation c pkh msg Sign_request >>= function
    | Ok () ->
        with_timeout
          (Lwt_unix.sleep request_timeout)
          (fun _ ->
             Lwt_utils_unix.Socket.recv_protected c.conn
               (result_encoding Sign.Response.encoding)) >>=
        begin function
          (* Receive a response from the remote signer *)
          | Ok (Ok res) ->
              add_req_status_ok st >>= fun () ->
              return res
          (* Timeout or receive an error from the remote signer *)
          | Ok (Error (err :: _)) | Error (err :: _) ->
              add_req_status_err st >>= fun () ->
              fail err
          | Ok (Error []) | Error [] ->
              add_req_status_err st >>= fun () ->
              failwith "Unknown error during signing"
        end
    | Error (err :: _) ->
        add_req_status_err st >>= fun () ->
        fail err
    | Error [] ->
        add_req_status_err st >>= fun () ->
        failwith "Unknown error during signing"

  let deterministic_nonce c pkh msg =
    let st = mk_req_status pkh c in
    signer_operation c pkh msg Deterministic_nonce_request >>= function
    | Ok () ->
        Lwt_utils_unix.Socket.recv_protected c.conn
          (result_encoding Deterministic_nonce.Response.encoding) >>= begin function
          | Ok (Ok res) ->
              add_req_status_ok st >>= fun () ->
              return res
          | Ok (Error (err :: _)) | Error (err :: _) ->
              add_req_status_err st >>= fun () ->
              fail err
          | Ok (Error []) | Error [] ->
              add_req_status_err st >>= fun () ->
              failwith "Unknown error during dertermining nonce"
        end >>=? fun res ->
        return res
    | Error (err :: _) ->
        add_req_status_err st >>= fun () ->
        fail err
    | Error [] ->
        add_req_status_err st >>= fun () ->
        failwith "Unknown error during dertermining nonce"


  let deterministic_nonce_hash c pkh msg =
    let st = mk_req_status pkh c in
    signer_operation c pkh msg Deterministic_nonce_hash_request >>= function
    | Ok () ->
        Lwt_utils_unix.Socket.recv_protected c.conn
          (result_encoding Deterministic_nonce_hash.Response.encoding) >>= begin function
          | Ok (Ok res) ->
              add_req_status_ok st >>= fun () ->
              return res
          | Ok (Error (err :: _)) | Error (err :: _) ->
              add_req_status_err st >>= fun () ->
              fail err
          | Ok (Error []) | Error [] ->
              add_req_status_err st >>= fun () ->
              failwith "Unknown error during dertermining nonce hash"
        end >>=? fun res ->
        return res
    | Error (err :: _) ->
        add_req_status_err st >>= fun () ->
        fail err
    | Error [] ->
        add_req_status_err st >>= fun () ->
        failwith "Unknown error during dertermining nonce hash"

  let supports_deterministic_nonces c pkh =
    Lwt_utils_unix.Socket.send_protected
      c.conn Request.encoding (Request.Supports_deterministic_nonces pkh) >>=? fun () ->
    Lwt_utils_unix.Socket.recv_protected c.conn
      (result_encoding Supports_deterministic_nonces.Response.encoding) >>=? fun res ->
    Lwt.return res

  let public_key c pkh =
    Lwt_utils_unix.Socket.send_protected
      c.conn Request.encoding (Request.Public_key pkh) >>=? fun () ->
    let encoding = result_encoding Public_key.Response.encoding in
    Lwt_utils_unix.Socket.recv_protected c.conn encoding >>=? fun res ->
    Lwt.return res

  module Tcp = struct

    let scheme = tcp_scheme

    let title =
      "Built-in tezos-signer using proxy signer through hardcoded tcp socket."

    let description =
      "Valid locators are of the form\n\
      \ - tcp-proxy://host:port/tz1..."

    let parse uri f =
      assert (Uri.scheme uri = Some scheme) ;
      trace (Invalid_uri uri) @@
      let pkh = Uri.path uri in
      let pkh =
        try String.(sub pkh 1 (length pkh - 1))
        with _ -> "" in
      Lwt.return
        (Signature.Public_key_hash.of_b58check pkh) >>=? fun pkh ->
      with_glob (fun path -> f path) pkh

    let public_key ?interactive:(_) uri =
      parse (uri : pk_uri :> Uri.t)
        public_key

    let neuterize uri =
      return (Client_keys.make_pk_uri (uri : sk_uri :> Uri.t))

    let public_key_hash ?interactive uri =
      public_key ?interactive uri >>=? fun pk ->
      return (Signature.Public_key.hash pk, Some pk)

    let sign ?watermark ?interactive:_ uri msg =
      parse (uri : sk_uri :> Uri.t) (fun path pkh ->
          sign ?watermark path pkh msg)

    let deterministic_nonce uri msg =
      parse (uri : sk_uri :> Uri.t) (fun path pkh ->
          deterministic_nonce path pkh msg)

    let deterministic_nonce_hash uri msg =
      parse (uri : sk_uri :> Uri.t) (fun path pkh ->
          deterministic_nonce_hash path pkh msg)

    let supports_deterministic_nonces uri =
      parse (uri : sk_uri :> Uri.t) (fun path pkh ->
          supports_deterministic_nonces path pkh)

  end

end
