(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Client_keys
open Clic

let group =
  { Clic.name = "dune" ;
    title = "Additionnal commands provided by DUNE" }

let sig_algo_arg =
  Clic.default_arg
    ~doc:"use custom signature algorithm"
    ~long:"sig"
    ~short:'s'
    ~placeholder:"ed25519|secp256k1|p256"
    ~default: "ed25519"
    (Signature.algo_param ())

let hash_algos = [
  "block",
  (fun contents -> Block_hash.to_b58check (Block_hash.hash_string contents));
  "operation",
  (fun contents -> Operation_hash.to_b58check (Operation_hash.hash_string contents));
  (*
  "operation_list", (Base58.Prefix.operation_list_hash, None) ;
  "operation_list_list", (Base58.Prefix.operation_list_list_hash) ;
  "protocol", Base58.Prefix.protocol_hash ;
  "context", (Base58.Prefix.context_hash, None) ;
  "ed25519_public_key", Base58.Prefix.ed25519_public_key_hash ;
  "secp256k1_public_key", Base58.Prefix.secp256k1_public_key_hash ;
  "p256_public_key", Base58.Prefix.p256_public_key_hash ;
  "cryptobox_public_key", Base58.Prefix.cryptobox_public_key_hash ;
  "ed25519_seed", Base58.Prefix.ed25519_seed ;
  "ed25519_public_key", Base58.Prefix.ed25519_public_key ;
  "ed25519_secret_key", Base58.Prefix.ed25519_secret_key ;
  "ed25519_signature", Base58.Prefix.ed25519_signature ;
*)
  "public_key",
  (fun contents ->
     let module Hash = Blake2B.Make(Base58)(struct
         let name = "hash"
         let title = "Blake2B hash"
         let b58check_prefix = Base58.Prefix.ed25519_public_key_hash
         let size = Some 20
       end) in
     Hash.to_b58check
       (Hash.hash_string contents));
  (*
  "secp256k1_secret_key", Base58.Prefix.secp256k1_secret_key ;
  "secp256k1_signature", Base58.Prefix.secp256k1_signature ;
  "p256_public_key", Base58.Prefix.p256_public_key ;
  "p256_secret_key", Base58.Prefix.p256_secret_key ;
  "p256_signature", Base58.Prefix.p256_signature ;
  "ed25519_encrypted_seed", Base58.Prefix.ed25519_encrypted_seed ;
  "secp256k1_encrypted_secret_key", Base58.Prefix.secp256k1_encrypted_secret_key ;
  "p256_encrypted_secret_key", Base58.Prefix.p256_encrypted_secret_key ;
  "generic_signature", Base58.Prefix.generic_signature ;
*)
  "chain_id",
  (fun contents ->
     Chain_id.to_b58check
       (Chain_id.hash_string contents));

  (*
  "secp256k1_element", Base58.Prefix.secp256k1_element ;
  "secp256k1_scalar", Base58.Prefix.secp256k1_scalar ;
  *)
]

let hash_algo_param () =
  Clic.parameter
    ~autocomplete:(fun _ -> return (List.map fst hash_algos))
    begin fun _ name ->
      try
        return (List.assoc name hash_algos)
      with Not_found ->
        failwith
          "Unknown hashing algorithm (%s). \
           Available: %s"
          name
          (String.concat ", " (List.map fst hash_algos))
    end

let hash_algo_arg =
  Clic.default_arg
    ~doc:"use custom b58hash prefix"
    ~long:"hash"
    ~short:'h'
    ~placeholder: (String.concat "| " (List.map fst hash_algos))
    ~default: "public_key"
    (hash_algo_param ())

let crypto_commands =
  [

    command ~group ~desc: "Generate a pair of keys and print them."
      (args1 sig_algo_arg)
      (prefixes [ "dune" ; "gen" ; "print" ; "keys" ]
       @@ stop)
      (fun algo (cctxt : Client_context.full) ->
         let (pkh, pk, sk) = Signature.generate_key ~algo () in
         let skloc = Tezos_signer_backends.Unencrypted.make_sk sk in
         cctxt#message "Hash: %a"
           Signature.Public_key_hash.pp pkh >>= fun () ->
         cctxt#message "Public Key: %a"
           Signature.Public_key.pp pk >>= fun () ->
         Secret_key.to_source skloc >>=? fun skloc ->
         cctxt#message "Secret Key: %s" skloc >>= fun () ->
         return_unit) ;

    command ~group ~desc: "Hash a string."
      (args1 hash_algo_arg)
      (prefixes [ "dune" ; "hash" ; "string" ]
       @@ Clic.string ~name:"data" ~desc:"string to hash"
       @@ stop)
      (fun f string (cctxt : Client_context.full) ->
         cctxt#message "Hash: %s" (f [string]) >>= fun () ->
         return_unit) ;

    command ~group ~desc: "Hash a file."
      (args1 hash_algo_arg)
      (prefixes [ "dune" ; "hash" ; "file" ]
       @@ Clic.string ~name:"data" ~desc:"file to hash"
       @@ stop)
      (fun f filename (cctxt : Client_context.full) ->
         Lwt_utils_unix.read_file filename >>= fun content ->
         cctxt#message "Hash: %s" (f [content]) >>= fun () ->
         return_unit) ;

    command ~group ~desc: "Print all the possible key hashes."
      no_options
      (prefixes [ "dune" ; "print" ; "key" ; "hashes" ]
       @@ Clic.string ~name:"keyhash" ~desc:"keyhash to print"
       @@ stop)
      (fun () keyhash (cctxt : Client_context.full) ->
         begin
           match Signature.Public_key_hash.of_b58check keyhash with
           | Ok key ->
               Lwt_list.iter_s (fun s ->
                   cctxt#message "Keyhash: %s" s)
                 (Signature.Public_key_hash.to_b58check_all key)
           | Error _ ->
               cctxt#message "Wrong key hash %S" keyhash
         end
         >>= fun () -> return_unit
      ) ;

    command ~group ~desc: "Base58check to hexa."
      no_options
      (prefixes [ "dune" ; "decode" ; "b58check" ]
       @@ Clic.string ~name:"b58check" ~desc:"b58check to print"
       @@ stop)
      (fun () s (cctxt : Client_context.full) ->
         begin
           match
             match Base58.decode s with

             | Some Ed25519.Public_key_hash.Data pkh ->
                 Some ( "ed25519 keyhash",
                        Ed25519.Public_key_hash.to_hex pkh )
             | Some Ed25519.Public_key.Data pkh ->
                 Some ( "ed25519 pubkey",
                        Ed25519.Public_key.to_hex pkh )
             | Some Ed25519.Secret_key.Data pkh ->
                 Some ( "ed25519 privkey",
                        Ed25519.Secret_key.to_hex pkh )
             | Some Ed25519.Data pkh ->
                 Some ( "ed25519 sig",
                        Ed25519.to_hex pkh )

             | Some Secp256k1.Public_key_hash.Data pkh ->
                 Some ( "secp256k1 keyhash",
                        Secp256k1.Public_key_hash.to_hex pkh )
             | Some Secp256k1.Public_key.Data pkh ->
                 Some ( "secp256k1 pubkey",
                        Secp256k1.Public_key.to_hex pkh )
             | Some Secp256k1.Secret_key.Data pkh ->
                 Some ( "secp256k1 privkey",
                        Secp256k1.Secret_key.to_hex pkh )
             | Some Secp256k1.Data pkh ->
                 Some ( "secp256k1 sig",
                        Secp256k1.to_hex pkh )

             | Some P256.Public_key_hash.Data pkh ->
                 Some ( "p256 keyhash",
                        P256.Public_key_hash.to_hex pkh )
             | Some P256.Public_key.Data pkh ->
                 Some ( "p256 pubkey",
                        P256.Public_key.to_hex pkh )
             | Some P256.Secret_key.Data pkh ->
                 Some ( "p256 privkey",
                        P256.Secret_key.to_hex pkh )
             | Some P256.Data pkh ->
                 Some ( "p256 sig",
                        P256.to_hex pkh )

             | Some Signature.Data pkh ->
                 Some ( "signature", Signature.to_hex pkh )
             | Some Chain_id.Data pkh ->
                 Some ( "chain_id", Chain_id.to_hex pkh )
             | Some Protocol_hash.Data pkh ->
                 Some ( "proto hash", Protocol_hash.to_hex pkh )
             | _ ->
                 None
           with
           | None ->
               cctxt#message "Hash %S not implemented or wrong" s
           | Some (kind, `Hex hex) ->
               cctxt#message "%s = %s %s" s kind hex
         end
         >>= fun () -> return_unit
      ) ;

  ]

let misc_commands =
  let output_arg =
    default_arg
      ~doc:"write to a file"
      ~long:"output"
      ~short:'o'
      ~placeholder:"path"
      ~default: "-"
      (parameter (fun _ -> function
           | "-" -> return Format.std_formatter
           | file ->
               let ppf = Format.formatter_of_out_channel (open_out file) in
               ignore Clic.(setup_formatter ppf Plain Full) ;
               return ppf)) in
  let input_arg ~default =
    default_arg
      ~doc:"read from a file"
      ~long:"input"
      ~short:'i'
      ~placeholder:"path"
      ~default
      (parameter (fun _ s -> return s))
  in

  let date_arg =
    arg
      ~doc:"specific date"
      ~long:"date in rfc3339 format"
      ~short:'d'
      ~placeholder:"date"
      (parameter (fun _ s -> match Time.Protocol.of_notation s with
           | Some d -> return d
           | None -> failwith "Bad date format, should in rfc3339 format"))
  in
  [

    command ~group ~desc: "The JSON corresponding to some hexa."
      (args2 (input_arg ~default:"protocol_parameters.bin") output_arg)
      (fixed [ "dune" ; "hexa" ; "to" ; "json" ])
      (fun (file, ppf) _cctxt ->
         Tezos_stdlib_unix.Lwt_utils_unix.read_file file >>= fun bytes ->
         let bytes = String.trim bytes in
         begin
           let bytes = MBytes.of_hex (`Hex bytes) in
           match Data_encoding.Binary.of_bytes Data_encoding.json bytes with
           | None ->
               Format.fprintf ppf "Error decoding@."
           | Some json ->
               Format.fprintf ppf "%a@." Data_encoding.Json.pp json
         end;
         return_unit);

    command ~group ~desc: "Find a base58 prefix."
      no_options
      (prefixes [ "dune" ; "find" ; "base58" ; "prefix" ]
       @@ Clic.string ~name:"target" ~desc:"target to generate"
       @@ prefixes [ "for" ; "size" ]
       @@ Clic.string ~name:"hash_size" ~desc:"size of hash"
       @@ stop)
      (fun () target hash_len _cctxt ->
         let hash_len = int_of_string hash_len in
         if not (Base58.Alphabet.all_in_alphabet
                   Base58.Alphabet.bitcoin target) then begin
           Format.eprintf "Invalid target %s. Chars must be in %a@."
             target
             Base58.Alphabet.pp Base58.Alphabet.bitcoin;
           return_unit
         end else
           let hash_bottom = String.make hash_len '\000' in
           let hash_top = String.make hash_len '\255' in
           let target_len = String.length target in
           let rec iter prefix n =
             if n = 256 then begin
               if String.length prefix = 1 then
                 Printf.eprintf "%d/256 tested\n%!"
                   (1 + int_of_char prefix.[0]);
               false
             end else
               let new_prefix = prefix ^ String.make 1 (char_of_int n) in
               if
                 String.sub
                   ( Base58.safe_encode (new_prefix ^ hash_bottom) )
                   0 target_len = target
                 &&
                 String.sub
                   ( Base58.safe_encode (new_prefix ^ hash_top) )
                   0 target_len = target
               then begin
                 Printf.eprintf "Found: \"";
                 for i = 0 to String.length new_prefix - 1 do
                   Printf.eprintf "\\%03d" (int_of_char new_prefix.[i])
                 done;
                 Printf.eprintf "\" (%d)\n"
                   (String.length
                      ( Base58.safe_encode (new_prefix ^ hash_bottom) ))
                 ;
                 true
               end else
                 ( if String.length new_prefix < target_len then
                     iter new_prefix 0
                   else
                     false ) || iter prefix (n+1)
           in
           ignore (iter "" 0);
           return_unit);

    command ~group ~desc: "Generate a Genesis block hash."
      (args2 date_arg output_arg)
      (fixed [ "dune" ; "generate" ; "genesis" ; "hash" ])
      (fun (date, ppf) _cctxt ->
         let prefix = "BLockGenesisGenesisGenesisGenesisGenesis" in
         let date =
           match date with
           | None ->
               Time.Protocol.of_notation_exn @@
               Lwt_main.run @@
               Lwt_process.pread_line @@
               Lwt_process.shell "TZ='AAA+1' date +%FT%TZ"
           | Some date -> date
         in
         let suffix =
           String.sub (Base58.raw_encode (Digest.string (Time.Protocol.to_notation date))) 0 11 in
         let genesis, date = match Base58.raw_decode (prefix ^ suffix) with
           | None -> assert false
           | Some p ->
               let p = String.sub p 0 (String.length p - 4) in
               Base58.safe_encode p, Time.Protocol.to_notation date
         in
         let _ = Block_hash.of_b58check_exn genesis in
         Format.fprintf ppf "Date: %s@." date;
         Format.fprintf ppf "Hash: %s@." genesis;
         return_unit);
  ]

let commands = crypto_commands @ misc_commands

(* If a directory "logs" exists in ~/.dune-client/, it will trigger
   permanent logging of requests in the background. It can be used in
   a debug setup to understand what happened after a command failed.
*)

let full_logger ~base_dir =
  let logs_dirname = Filename.concat base_dir "logs" in
  if Sys.file_exists logs_dirname then
    let pid = Unix.getpid () in
    let date = Systime_os.now () |> Ptime.to_rfc3339 in
    let oc = Printf.kprintf open_out "%s/%s-%s-%d.log"
        logs_dirname (Filename.basename Sys.argv.(0)) date pid in
    Array.iteri (fun i arg ->
        Printf.fprintf oc "arg[%d] = %S\n" i arg) Sys.argv;
    Some ( Format.formatter_of_out_channel oc )
  else
    None
